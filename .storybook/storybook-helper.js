import React from 'react'

export const ShowCaseBox = ({ children, style, backgroundColor }) => {
  const caseStyle = {
    backgroundColor: !backgroundColor ? 'white' : backgroundColor,
    borderWidth: '1px',
    borderStyle: 'solid',
    borderColor: 'black',
  }
  return <div style={{ ...caseStyle, ...style }}>{children}</div>
}
