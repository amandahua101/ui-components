const filterOutWebpackEslintLoader = require('./webpackmod.js')

module.exports = {
  webpackFinal: filterOutWebpackEslintLoader,
  stories: ['../src/stories/**/*.stories.(js|mdx)'],
  addons: [
    '@storybook/addon-a11y/register',
    '@storybook/addon-knobs/register',
    '@storybook/addon-actions/register',
    '@storybook/addon-docs',
  ],
}
