// we don't need no stinkin eslint-loader in storybook plz!
const filterOutWebpackEslintLoader = config => {
  let eslintLoaderIndices = []
  const { rules } = config.module
  rules.map((rule, i) => {
    if (
      rule &&
      rule.use &&
      rule.use.length > 0 &&
      rule.use.filter(
        ({ loader }) => loader && loader.includes('eslint-loader')
      ).length > 0
    ) {
      console.log(
        `webpack-config: eslint-loader found in modules.rules[${i}], ignoring it`
      )
      eslintLoaderIndices.push(i)
    }
  })
  const noEslintLoader = rules.filter((_, i) => {
    return !eslintLoaderIndices.includes(i)
  })
  return {
    ...config,
    module: {
      ...config.module,
      rules: noEslintLoader,
    },
  }
}

module.exports = filterOutWebpackEslintLoader
