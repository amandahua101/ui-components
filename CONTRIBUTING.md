# CONTRIBUTING to @linguala/ui-components

TLDR: use gitlab merge requests for feature branches and assign to maintainer when you are finished

## prerequisites

In order to let others know who changed what, set up git properly.

### set user email & your name

> note: `<` or `>` are placeholders, so insert your email and name instead


`git config user.email "<your.name>@linguala.com"`

`git config user.name "<your full name>"`

### changes in source code

If you want to change something in ui-components you have multiple ways to start your contribution.

#### start with gitlab-issues

1. Use gitlab-issues (either create a new one or base your work on an existing issue) and create a merge request and branch based on the issue. (Note: you can select the branch-name in this step)
2. Fetch the changes from server and checkout the remotely created branch from gitlab by running `git fetch` and `git checkout 1-my-issuename`

#### start locally with a feature-branch

1. Start locally and create a new feature-branch based on the latest master branch
2. (optionally) make sure you fetch and merge latests changes by doing `git pull` in a clean master branch beforehand to have all integrated changes.
3. `git checkout -b feature/my-new-feature-branch`

#### commit changes locally

1. Edit some files and test your changes (have a look at storybook, eslint & jest output)
2. Commit files (make sure to commit atomic changes, do not use `git commit -a` blindly)

#### push your commits to gitlab

1. Push your changes back with `git push`
2. (if you started locally you need to assign to which remote branch and git will give you the command)
3. You should be able to create a merge request based on the pushed branch if you did not already do that

#### bring your changes into master with a merge request

1. assign the merge request to a maintainer to be reviewed (the person assigned will get a notification)
2. remove `WIP:` if you are finished (work in progress flag)
3. make sure your changes don't break any tests or builds (gitlab-ci and husky will make sure you know)

Congrats!
