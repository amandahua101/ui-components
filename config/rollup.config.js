// Rollup plugins.
import buble from 'rollup-plugin-buble'
import cjs from 'rollup-plugin-commonjs'
import globals from 'rollup-plugin-node-globals'
import replace from 'rollup-plugin-replace'
import resolve from 'rollup-plugin-node-resolve'
import image from 'rollup-plugin-img'
import { uglify } from 'rollup-plugin-uglify'
import { terser } from 'rollup-plugin-terser'
import url from 'rollup-plugin-url'
import { plugin as analyze } from 'rollup-plugin-analyzer'

import pkg from '../package.json'

const _uglify = format => (format === 'es' ? terser : uglify)

export default [
  {
    file: pkg.module,
    format: 'es',
    sourcemap: true,
  },
  {
    file: pkg.main,
    format: 'cjs',
    sourcemap: true,
  },
].map(output => ({
  input: 'src/lib/index.js',
  output,
  external: ['react', 'react-dom', 'styled-components'],
  plugins: [
    resolve({
      browser: true,
      mainFields: ['module', 'main'],
      extensions: ['.js', '.jsx', '.json'],
    }),
    image({
      // do not use separate asset files. Figure out how to bundle asset files properly #44
      limit: false,
    }),
    url({
      // do not use separate asset files. Figure out how to bundle asset files properly #44
      limit: false,
      include: ['**/*.woff2'],
      emitFiles: true, // defaults to true
    }),
    cjs({
      namedExports: {
        // left-hand side can be an absolute path, a path
        // relative to the current directory, or the name
        // of a module in node_modules
        'node_modules/classnames/index.js': ['default'],
      },
      exclude: 'node_modules/process-es6/**',
      include: [
        'node_modules/create-react-class/**',
        'node_modules/fbjs/**',
        'node_modules/object-assign/**',
        'node_modules/react/**',
        'node_modules/react-dom/**',
        'node_modules/react-is/**',
        'node_modules/prop-types/**',
        'node_modules/hoist-non-react-statics/**',
        'node_modules/is-function/**',
        'node_modules/is-plain-object/**',
        'node_modules/stylis/**',
        'node_modules/isobject/**',
      ],
    }),
    buble({
      objectAssign: 'Object.assign',
      transforms: { dangerousTaggedTemplateString: true },
    }),
    globals(),
    replace({ 'process.env.NODE_ENV': JSON.stringify('development') }),
    process.env.NODE_ENV === 'production' && _uglify(output.format)(),
    process.env.NODE_ENV === 'ci' && analyze(),
  ],
}))
