import styled from 'styled-components'

export const ContentBox = styled.div`
  background-color: seashell;
  box-shadow: 0 4px 5px 0 rgba(0, 0, 0, 0.2), 0 3px 14px 3px rgba(0, 0, 0, 0.12),
    0 8px 10px 1px rgba(0, 0, 0, 0.14);
`
