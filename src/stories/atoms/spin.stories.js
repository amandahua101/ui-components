/* eslint-disable sort-exports/sort-exports */
import React from 'react'
import { withA11y } from '@storybook/addon-a11y'
import { withKnobs } from '@storybook/addon-knobs'

import { Bubble, Button, Icon, PinShape, Spin, Text, Time } from '../../lib'

export default {
  title: 'Atoms/Spin',
  decorators: [withKnobs, withA11y],
}

export const ButtonLinear = () => (
  <Spin>
    <Button size={12} disabled value="loading..." />
  </Spin>
)

ButtonLinear.story = {
  name: 'Button/linear',
}

export const BubbleLinear = () => (
  <Spin>
    <Bubble>loading...</Bubble>
  </Spin>
)

BubbleLinear.story = {
  name: 'Bubble/linear',
}

export const BubbleWobbly = () => (
  <Spin wobbly>
    <Bubble>loading...</Bubble>
  </Spin>
)

BubbleWobbly.story = {
  name: 'Bubble/wobbly',
}

export const PinShapeReverse = () => (
  <Spin reverse>
    <PinShape rotation={-170}>
      <Icon alt="Time Icon for loading indication" icon={Time} />
    </PinShape>
  </Spin>
)

PinShapeReverse.story = {
  name: 'PinShape/reverse',
}

export const NestedTextWobblyReverseFast = () => (
  <Spin
    fast
    reverse
    style={{
      height: '2rem',
      width: '2rem',
      border: '1px solid gray',
      backgroundColor: 'white',
      borderRadius: '50%',
    }}
  >
    <Spin wobbly fast>
      <Text size={3}>
        <span role="img" aria-label="loading">
          🔧
        </span>
      </Text>
    </Spin>
  </Spin>
)

NestedTextWobblyReverseFast.story = {
  name: 'nested Text/wobbly+reverse+fast',
}
