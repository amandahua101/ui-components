import React from 'react'
import { boolean, number, withKnobs } from '@storybook/addon-knobs'
import { withA11y } from '@storybook/addon-a11y'

import { Lens } from '../../../lib'

export default {
  title: 'Atoms/Shape',
  decorators: [withKnobs, withA11y],
}

export const LensBorder = () => (
  <Lens
    size={number('Size in units', 10, { min: 2, max: 36, step: 2 })}
    rotation={number('Rotation in °', 0, { min: -360, max: 360, step: 15 })}
    danger={boolean('danger')}
    disabled={boolean('disabled')}
  />
)
