import React from 'react'
import { boolean, number, withKnobs } from '@storybook/addon-knobs'
import { withA11y } from '@storybook/addon-a11y'
import themeGet from '@styled-system/theme-get'

import { PinShape } from '../../../lib'
import Container from '../../helpers/Container.js'

export default {
  title: 'Atoms/Shape',
  decorators: [withKnobs, withA11y],
}

export const PinFullBorderVariants = () => (
  <div>
    <Container>
      <PinShape
        full
        rotation={number('Rotation in °', 0, {
          min: -360,
          max: 360,
          step: 15,
          range: true,
        })}
        size={number('Size in units', 5, {
          range: true,
          min: 2,
          max: 14,
          step: 1,
        })}
        danger={boolean('danger')}
        disabled={boolean('disabled')}
      />
      <PinShape
        border
        rotation={number('Rotation in °', 0, {
          min: -360,
          max: 360,
          step: 15,
          range: true,
        })}
        size={number('Size in units', 5, {
          range: true,
          min: 2,
          max: 14,
          step: 1,
        })}
        danger={boolean('danger')}
        disabled={boolean('disabled')}
      />
    </Container>
    <Container
      style={{
        paddingTop: themeGet('spacing.medium'),
      }}
    >
      <span>full</span>
      <span>border</span>
    </Container>
  </div>
)

PinFullBorderVariants.story = {
  name: 'Pin/full+border',
}
