import React from 'react'
import { withA11y } from '@storybook/addon-a11y'

import { Colorable } from '../../lib'

export default {
  title: 'Atoms/Color',
  decorators: [withA11y],
}

export const _Colorable = () => (
  <div>
    <Colorable full>primary</Colorable>
    <Colorable full secondary>
      secondary
    </Colorable>
    <Colorable full disabled>
      disabled
    </Colorable>
    <Colorable full danger>
      danger
    </Colorable>
  </div>
)
