import React, { useState } from 'react'
import { withKnobs } from '@storybook/addon-knobs'
import { withA11y } from '@storybook/addon-a11y'
import styled from 'styled-components'

import {
  Button,
  Form,
  Select,
  Selector,
  SingleLineInput,
  Toggle,
} from '../../lib'

const StyledColumn = styled.div`
  display: flex;
  flex-direction: column;
`

function SimpleForm() {
  const initFormState = {
    input1: {
      label: 'Test Input',
    },
    selector1: {
      placeholder: 'Pick a color',
      options: ['red', 'white', 'black', 'white is not a color'],
    },
  }

  const [state, setState] = useState(initFormState)
  const [key, setKey] = useState(0)

  const ShowState = () => {
    return (
      <React.Fragment>
        <p>State:</p>
        <pre>{JSON.stringify(state, null, 4)}</pre>
      </React.Fragment>
    )
  }

  return (
    <React.Fragment>
      <StyledColumn>
        <Form
          initFormState={state}
          key={key}
          handleFormSubmit={formState =>
            alert(JSON.stringify(formState, null, 4))
          }
          handleFormChange={formState => setState(formState)}
        >
          <SingleLineInput name="input1" />
          <Selector name="selector1" />
          <Button value="Submit" />
        </Form>
      </StyledColumn>
      <StyledColumn>
        <ShowState />
        <Button
          value="RESET"
          onClick={() => {
            initFormState.input1.value = 'Goodbye'
            setState(initFormState)
            setKey(k => k + 1)
          }}
        />
      </StyledColumn>
    </React.Fragment>
  )
}

function AllElementsForm() {
  const initFormState = {
    user: {
      label: 'Username',
    },
    password: {
      label: 'Password',
      type: 'password',
    },
    numberSelector: {
      placeholder: 'Select your Numbers',
      options: [1, 2, 3, 4, 5, 6, 7, 8, 9],
      multiple: true,
    },
    toggleTest: {
      value: true,
    },
    languageSelect: {
      label: 'Please select a language',
      options: ['Languages', 'English', 'German', 'French', 'Italian'],
    },
  }

  const [state, setState] = useState({})
  const [key, setKey] = useState(0)

  const ShowState = () => (
    <React.Fragment>
      <p>State:</p>
      {JSON.stringify(state, null, 4)}
    </React.Fragment>
  )

  return (
    <React.Fragment>
      <StyledColumn>
        <Form
          handleFormSubmit={formState => alert(JSON.stringify(state, null, 4))}
          handleFormChange={formState => setState(formState)}
          initFormState={initFormState}
          key={key}
        >
          <SingleLineInput name="user" />
          <SingleLineInput name="password" />
          <Select name="languageSelect" />
          <Selector name="numberSelector" />
          <Toggle name="toggleTest" />
          <Button value="login" />
        </Form>
      </StyledColumn>
      <StyledColumn>
        <ShowState />
        <Button
          value="RESET"
          onClick={() => {
            setState(initFormState)
            setKey(k => k + 1)
          }}
        />
      </StyledColumn>
    </React.Fragment>
  )
}

export default {
  title: 'Atoms/Forms',
  decorators: [withKnobs, withA11y],
}

export const CompleteForm = () => (
  <React.Fragment>
    <AllElementsForm />
  </React.Fragment>
)

export const _SimpleForm = () => (
  <React.Fragment>
    <SimpleForm />
  </React.Fragment>
)
