import React from 'react'
import { withA11y } from '@storybook/addon-a11y'
import { withKnobs } from '@storybook/addon-knobs'

import { Icon, Spinner, Time } from '../../lib'

export default {
  title: 'Molecules/Spinner',
  decorators: [withKnobs, withA11y],
}

export const _Spinner = () => (
  <Spinner>
    <Icon alt="Time Icon for loading indication" icon={Time} />
  </Spinner>
)
