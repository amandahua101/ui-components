import React from 'react'
import styled from 'styled-components'

import { PinShape as Pin } from '../../lib/index.atoms.js'
import { TriShape } from '../../lib/index.molecules.js'
import { ContentBox } from '../utils'

const CenteredText = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  margin: auto;
  height: 100%;
  width: 100%;
`

const T = CenteredText

export default {
  title: 'Molecules/TriShape',
}

export const TriShapePinDefault = () => (
  <ContentBox>
    <TriShape
      Component={Pin}
      left={<T>L</T>}
      middle={<T>M</T>}
      right={<T>R</T>}
    />
  </ContentBox>
)

TriShapePinDefault.story = {
  name: 'TriShape Pin (default)',
}

export const TriShapePinSizeable = () => (
  <ContentBox>
    <TriShape
      initSize={24}
      Component={Pin}
      left={<T>left</T>}
      middle={<T>middle</T>}
      right={<T>right</T>}
    />
  </ContentBox>
)

TriShapePinSizeable.story = {
  name: 'TriShape Pin (sizeable)',
}
