import React from 'react'
import { withA11y } from '@storybook/addon-a11y'

import { Accordion, Button, Title } from '../../lib'

export default {
  title: 'Molecules/Accordion',
  decorators: [withA11y],
}

export const _Accordion = () => <Accordion header={AccHeader} body={AccBody} />

const AccHeader = () => <Title>This is an accordion title</Title>

const AccBody = () => (
  <>
    <p>
      Away ipsum dolor sit for sure, consectetuer adipiscing my shizz. Nullam
      crazy velit, aliquet volutpat, suscipizzle the bizzle, gravida vizzle,
      arcu. Pellentesque eget tortizzle. Sizzle eros. Fusce at shut the shizzle
      up dapibizzle turpis pot funky fresh. Maurizzle pellentesque nibh et
      turpizzle. Brizzle izzle tortizzle. Pellentesque dope rhoncizzle gangster.
      In hac habitasse platea dictumst. Funky fresh dapibizzle. Pizzle tellizzle
      shizznit, pretizzle eu, mattizzle shut the shizzle up, cool vitae, nunc.
      Check out this suscipizzle. Shizznit semper velizzle sed i saw beyonces
      tizzles and my pizzle went crizzle.
    </p>
    <Button>Was this helpful?</Button>
  </>
)
