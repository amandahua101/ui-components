/* eslint-disable sort-exports/sort-exports */
import React from 'react'
import { boolean, number, withKnobs } from '@storybook/addon-knobs'
import { withA11y } from '@storybook/addon-a11y'

import { Currency, Icon, Lens, Logo, Text } from '../../../lib'

export default {
  title: 'Molecules/Lens',
  decorators: [withKnobs, withA11y],
}

export const LogoLensComposition = () => (
  <Lens
    size={number('Size in units', 10, {
      range: true,
      min: 1,
      max: 42,
      step: 0.5,
    })}
    rotation={number('Rotation in °', 0, {
      range: true,
      min: -360,
      max: 360,
      step: 1,
    })}
    danger={boolean('danger')}
    disabled={boolean('disabled')}
  >
    <Logo />
  </Lens>
)

LogoLensComposition.story = {
  name: 'Logo + Lens Composition',
}

export const IconLensComposition = () => (
  <Lens
    size={number('Size in units', 2, {
      range: true,
      min: 1,
      max: 12,
      step: 0.5,
    })}
    rotation={number('Rotation in °', 0, {
      range: true,
      min: -360,
      max: 360,
      step: 15,
    })}
    danger={boolean('danger')}
    disabled={boolean('disabled')}
  >
    <Icon icon={Currency} alt="Money" />
  </Lens>
)

IconLensComposition.story = {
  name: 'Icon + Lens Composition',
}

export const TextLensComposition = () => (
  <Lens
    size={number('Size in units', 2, {
      range: true,
      min: 1,
      max: 12,
      step: 0.5,
    })}
    rotation={number('Rotation in °', 0, {
      range: true,
      min: -360,
      max: 360,
      step: 15,
    })}
    danger={boolean('danger')}
    disabled={boolean('disabled')}
  >
    <Text>Linguala</Text>
  </Lens>
)

TextLensComposition.story = {
  name: 'Text + Lens Composition',
}
