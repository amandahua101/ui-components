/* eslint-disable sort-exports/sort-exports */
import React from 'react'
import { boolean, number, withKnobs } from '@storybook/addon-knobs'
import { withA11y } from '@storybook/addon-a11y'

import { Currency, Icon, Logo, PinShape, Text } from '../../../lib'

export default {
  title: 'Molecules/Pin',
  decorators: [withKnobs, withA11y],
}

export const LogoPinComposition = () => (
  <PinShape
    size={number('Size in units', 2, {
      range: true,
      min: 1,
      max: 12,
      step: 0.5,
    })}
    rotation={number('Rotation in °', 0, {
      range: true,
      min: -360,
      max: 360,
      step: 15,
    })}
    danger={boolean('danger')}
    disabled={boolean('disabled')}
  >
    <Logo />
  </PinShape>
)

LogoPinComposition.story = {
  name: 'Logo + Pin Composition',
}

export const IconPinComposition = () => (
  <PinShape
    size={number('Size in units', 2, {
      range: true,
      min: 1,
      max: 12,
      step: 0.5,
    })}
    rotation={number('Rotation in °', 0, {
      range: true,
      min: -360,
      max: 360,
      step: 15,
    })}
    danger={boolean('danger')}
    disabled={boolean('disabled')}
  >
    <Icon icon={Currency} alt="Money" />
  </PinShape>
)

IconPinComposition.story = {
  name: 'Icon + Pin Composition',
}

export const TextPinComposition = () => (
  <PinShape
    size={number('Size in units', 2, {
      range: true,
      min: 1,
      max: 12,
      step: 0.5,
    })}
    rotation={number('Rotation in °', 0, {
      range: true,
      min: -360,
      max: 360,
      step: 15,
    })}
    danger={boolean('danger')}
    disabled={boolean('disabled')}
  >
    <Text>Linguala</Text>
  </PinShape>
)

TextPinComposition.story = {
  name: 'Text + Pin Composition',
}
