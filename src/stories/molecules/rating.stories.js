import React from 'react'
import { withA11y } from '@storybook/addon-a11y'
import { number, withKnobs } from '@storybook/addon-knobs'

import { FiveStarRating } from '../../lib/index.molecules'

export default {
  title: 'Molecules/Rating',
  decorators: [withA11y, withKnobs],
}

export const _FiveStarRating = () => (
  <FiveStarRating
    rating={number('count of stars', 2, {
      range: false,
      min: 1,
      max: 5,
      step: 0.5,
    })}
  />
)

_FiveStarRating.story = {
  name: 'FiveStarRating',
}
