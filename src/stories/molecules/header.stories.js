import React from 'react'

import { Logo, Title } from '../../lib/index.atoms.js'

export default {
  title: 'Molecules/Header',
}

export const Marktplatz = () => (
  <div>
    <Logo />
    <Title>marktplatz für sprachservices</Title>
  </div>
)
