import React from 'react'

import { ProfileBubble } from '../../lib/index.organisms.js'

export default {
  title: 'Organisms/ProfileBubble',
}

export const ProfileBubbleDefault = () => (
  <ProfileBubble
    pictureURL={
      'http://www.papersurfer.com/wp-content/uploads/2007/09/babelfish.jpg'
    }
  />
)

ProfileBubbleDefault.story = {
  name: 'ProfileBubble/default',
}

export const ProfileBubbleLuigi = () => (
  <ProfileBubble
    name={'Luigi'}
    languageCount={3}
    rating={1.5}
    servicesIDs={['karting', 'mushrooms', 'mario']}
    pictureURL={
      'https://yt3.ggpht.com/-kqCAcE59qSo/AAAAAAAAAAI/AAAAAAAAAAA/zy4skyAcVCo/s900-c-k-no-mo-rj-c0xffffff/photo.jpg'
    }
    location={{ name: 'Japan', latlng: [11, 12] }}
    contact={'マリオ'}
    favorite={'キノコ'}
  />
)

ProfileBubbleLuigi.story = {
  name: 'ProfileBubble/Luigi',
}
