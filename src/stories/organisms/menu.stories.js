import React from 'react'

import { Menu } from '../../lib/index.organisms.js'

const data = {
  headings: [
    { item: 'Branche' },
    { item: 'Profil', selected: true },
    // { item: 'third' },
  ],
  entries: [
    { item: 'Porträt' },
    { item: 'Banner', selected: true },
    { item: 'Bezahlung' },
    { item: 'Kontakt' },
    { item: 'Blog' },
    // { item: 'Very long text' },
    // {
    //   item: 'This text is an extremely long text that makes the menu very wide',
    // },
  ],
}

export default {
  title: 'Organisms/Menu',
}

export const _Menu = () => (
  <Menu headings={data.headings} entries={data.entries} />
)
