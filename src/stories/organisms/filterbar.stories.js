import React from 'react'
import styled from 'styled-components'
import { action } from '@storybook/addon-actions'

import { FilterBar } from '../../lib/index.organisms.js'

const Dropdown = styled.select`
  display: inline-block;
  -moz-appearance: none;
  background-color: transparent;
  border-style: none;
  border-radius: 1em;
  min-width: 8em;
`

const Form = ({ children, ...props }) => <form {...props}>{children}</form>

const filters = [
  {
    id: 'from_language',
    placeholder: 'von Sprache',
    options: [
      { value: 'de', label: 'Deutsch' },
      { value: 'en', label: 'Englisch' },
    ],
  },
  {
    id: 'to_language',
    placeholder: 'zu Sprache',
    options: [
      { value: 'en', label: 'Englisch' },
      { value: 'de', label: 'Deutsch' },
    ].reverse(),
  },
  {
    id: 'service',
    placeholder: 'service',
    options: [
      {
        optgroup: {
          label: 'Übersetzung',
          options: [
            {
              value: 'texttranslation',
              label: 'Textübersetzung',
            },
            {
              value: 'audiotranslation',
              label: 'Audioübersetzung',
            },
            {
              value: 'localisation',
              label: 'Lokalisierung',
            },
          ],
        },
      },
      {
        optgroup: {
          label: 'Dolmetschen',
          options: [
            {
              value: 'conferencedolmetsch',
              label: 'Konferenzdolmetschen',
            },
            {
              value: 'simultaneousdolmetsch',
              label: 'Simultandolmetschen',
            },
            {
              value: 'consecutivedolmetsch',
              label: 'Konsekutivdolmetschen',
            },
          ],
        },
      },
    ],
  },
  {
    id: 'fachgebiete',
    placeholder: 'fachgebiete',
    options: [
      { value: 'wirtschaft', label: 'Wirtschaft' },
      {
        value: 'marketing',
        label: 'marketing',
      },
      {
        value: 'recht',
        label: 'recht',
      },
      {
        value: 'technologie',
        label: 'technologie',
      },
      { value: 'medizin', label: 'medizin' },
      {
        value: 'sonstige fachgebiete',
        label: 'sonstige fachgebiete',
      },
    ],
  },
]

const detailedFilters = [
  {
    id: 'land1',
    placeholder: 'Land',
    options: [],
  },
  {
    id: 'land2',
    placeholder: 'Land',
    options: [],
  },
  {
    id: 'region1',
    placeholder: 'Region',
    options: [],
  },
  {
    id: 'region2',
    placeholder: 'Region',
    options: [],
  },
  {
    id: 'ort1',
    placeholder: 'Ort',
    options: [],
  },
  {
    id: 'ort2',
    placeholder: 'Ort',
    options: [],
  },
]

const OptionsMapper = ({ value, label }, key) => (
  <option key={key} value={value}>
    {label}
  </option>
)

const Filter = ({ id, placeholder, options }, key) => (
  <Dropdown
    key={key}
    name={id}
    placeholder={placeholder}
    onChange={action('dropdown changed selection')}
  >
    <option key={0} value="" disabled selected hidden>
      {placeholder}
    </option>
    {options.map(({ optgroup, ...option }, key) => {
      if (optgroup) {
        const { label, options } = optgroup
        return (
          <optgroup key={key + 1} label={label}>
            {options.map(OptionsMapper)}
          </optgroup>
        )
      } else {
        return OptionsMapper(option)
      }
    })}
  </Dropdown>
)

export default {
  title: 'Organisms/FilterBar',
}

/* eslint-disable sort-exports/sort-exports */
export const _FilterBar = () => (
  <FilterBar
    Form={Form}
    searchFilters={filters.map(Filter)}
    defaultOpen={false}
  />
)

export const _FilterBarExtended = () => (
  <FilterBar
    Form={Form}
    searchFilters={filters.map(Filter)}
    extendedFilters={detailedFilters.map(Filter)}
    onDetailChange={(isOpen, setOpen) => {
      setOpen(!isOpen)
    }}
    onDetailToggleEffect={action('detailview has been toggled')}
  />
)

export const _FilterBarDetailOnly = () => (
  <FilterBar searchFilters={[]} extendedFilters={detailedFilters.map(Filter)} />
)
