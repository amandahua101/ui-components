import styled from 'styled-components'

const Container = styled.div`
  display: flex;
  justify-content: space-evenly;
  min-width: 300px;
  width: 90vw;
`

export default Container
