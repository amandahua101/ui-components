const green = '#1CB569'
const blue = '#4A90E2'
const grey = '#9B9B9B'
const red = '#BB2D00'
const green10 = '#e8f7f0'
const blue10 = '#ecf3fc'
const darkgrey = '#5A719A'

export { blue, blue10, green, green10, grey, red, darkgrey }

export default {
  blue,
  blue10,
  green,
  grey,
  red,
  green10,
  darkgrey,
}
