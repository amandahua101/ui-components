import {
  blue,
  blue10,
  darkgrey,
  green,
  green10,
  grey,
  red,
} from '../assets/colors'

const theme = {
  colors: {
    primary: green,
    secondary: blue,
    danger: red,
    idle: grey,
    defaultText: darkgrey,
    background: {
      agency: green10,
      agent: blue10,
    },
  },
  fonts: {
    main: 'Linguala-Text, Helvetica, Arial, sans-serif',
    title: 'Linguala-Title, Helvetica, Arial, sans-serif',
    password: 'Linguala-Password, Helvetica, Arial, sans-serif',
    primaryColor: 'white',
  },
  fontSizes: {
    body: '16px',
    p: '1rem',
    h1: '3rem',
    h2: '2.5rem',
    h3: '2rem',
  },
  spacing: {
    small: '0.5rem',
    medium: '1rem',
    large: '2rem',
  },
  units: {
    size: 20,
    unit: 'px',
  },
  transitions: {
    main: 'cubic-bezier(0.4, 0, 0.2, 1)',
    wobbly: 'cubic-bezier(0.68, -0.55, 0.27, 1.55) ',
  },
  shadows: {
    low:
      '0 1px 5px 0 rgba(0, 0, 0, 0.2), 0 3px 4px 0 rgba(0, 0, 0, 0.12), 0 2px 4px 0 rgba(0, 0, 0, 0.14)',
    medium:
      '0px 4px 5px 0 rgba(0, 0, 0, 0.2), 0 3px 14px 3px rgba(0, 0, 0, 0.12), 0 8px 10px 1px rgba(0, 0, 0, 0.14)',
    high:
      '0 8px 10px 0 rgba(0, 0, 0, 0.2), 0 6px 30px 5px rgba(0, 0, 0, 0.12), 0 16px 24px 2px rgba(0, 0, 0, 0.14)',
  },
  border: {
    radius: '3rem',
  },
}

export default theme
