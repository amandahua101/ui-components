import { css } from 'styled-components'

export const centerMixin = css`
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  word-wrap: anywhere;
`
