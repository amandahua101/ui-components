import { css } from 'styled-components'
import themeGet from '@styled-system/theme-get'

const linkColor = css`
  color: ${themeGet('colors.primary')}
  &:hover {
    color: ${themeGet('colors.secondary')}
  }
  text-decoration: none;
`

export default linkColor
