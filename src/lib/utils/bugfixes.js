// Workaround for a Bug:
//  when importing assets added quotes are passed to rendered props
//  with this function we remove it
const fixNodeImports = str => {
  return str[0] === '"' && str[str.length - 1] === '"'
    ? str.substring(1, str.length - 1)
    : str
}

export { fixNodeImports }
