import theme from '../styles/theme'

const { size: defaultBase, unit: defaultUnit } = theme.units

export const unit = size => unitLength(defaultBase, defaultUnit)(size)
export const unitLength = (base, unit) => {
  const _unit = unit || defaultUnit
  const _base = base || defaultBase
  return (factor = 1) => `${factor * _base}${_unit}`
}
