import React from 'react'

const SvgMenu = props => (
  <svg viewBox="0 0 100 100" fill="none" {...props}>
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M50 100c27.614 0 50-22.386 50-50S77.614 0 50 0 0 22.386 0 50s22.386 50 50 50zM28.4 25a5.4 5.4 0 0 0 0 10.8h43.2a5.4 5.4 0 0 0 0-10.8H28.4zM23 50.2a5.4 5.4 0 0 1 5.4-5.4h43.2a5.4 5.4 0 0 1 0 10.8H28.4a5.4 5.4 0 0 1-5.4-5.4zm5.4 14.4a5.4 5.4 0 1 0 0 10.8h43.2a5.4 5.4 0 0 0 0-10.8H28.4z"
      fill="currentcolor"
    />
  </svg>
)

export default SvgMenu
