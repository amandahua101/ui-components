import React from 'react'

const SvgExpand = props => (
  <svg viewBox="0 0 100 100" fill="none" {...props}>
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M50 100c27.614 0 50-22.386 50-50S77.614 0 50 0 0 22.386 0 50s22.386 50 50 50zM30.311 46.342l-5.622-7.028L50 19.065l25.311 20.249-5.622 7.028L50 30.59 30.311 46.34zm39.378 8.214l5.622 7.028-25.31 20.249-25.312-20.25 5.622-7.027 19.69 15.751 19.688-15.751z"
      fill="currentcolor"
    />
  </svg>
)

export default SvgExpand
