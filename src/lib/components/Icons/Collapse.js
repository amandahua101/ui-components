import React from 'react'

const SvgCollapse = props => (
  <svg viewBox="0 0 100 100" fill="none" {...props}>
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M50 100c27.614 0 50-22.386 50-50S77.614 0 50 0 0 22.386 0 50s22.386 50 50 50zm19.689-78.686l5.622 7.028-25.31 20.249-25.312-20.25 5.622-7.027 19.69 15.751 19.688-15.751zm-39.378 58.27l-5.622-7.028L50 52.307l25.311 20.249-5.622 7.028L50 63.833l-19.689 15.75z"
      fill="currentcolor"
    />
  </svg>
)

export default SvgCollapse
