import React from 'react'

const SvgLogin = props => (
  <svg viewBox="0 0 100 100" fill="none" {...props}>
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M100 50c0 27.614-22.386 50-50 50S0 77.614 0 50 22.386 0 50 0s50 22.386 50 50zM63.965 16.278a36.5 36.5 0 0 0-39.777 7.913l6.364 6.364a27.5 27.5 0 1 1 0 38.89l-6.364 6.364a36.5 36.5 0 1 0 39.778-59.53zm-25.09 21.964l6.364-6.364L63.361 50 45.239 68.122l-6.364-6.364 7.258-7.258H15.5v-9h30.633l-7.258-7.258z"
      fill="currentcolor"
    />
  </svg>
)

export default SvgLogin
