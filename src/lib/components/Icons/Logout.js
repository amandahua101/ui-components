import React from 'react'

const SvgLogout = props => (
  <svg viewBox="0 0 100 100" fill="none" {...props}>
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M100 50c0 27.614-22.386 50-50 50S0 77.614 0 50 22.386 0 50 0s50 22.386 50 50zM36.033 16.278a36.5 36.5 0 0 1 39.777 7.913l-6.364 6.364a27.5 27.5 0 1 0 0 38.89l6.364 6.364a36.5 36.5 0 1 1-39.777-59.53zm16.982 21.964l6.364-6.364L77.501 50 59.379 68.122l-6.364-6.364 7.258-7.258H29.64v-9h30.633l-7.258-7.258z"
      fill="currentcolor"
    />
  </svg>
)

export default SvgLogout
