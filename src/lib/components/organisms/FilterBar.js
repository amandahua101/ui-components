import React, { useState } from 'react'
import propTypes from 'prop-types'
import styled, { css } from 'styled-components'
import themeGet from '@styled-system/theme-get'

import { Button } from '../../index.atoms'
import Bubble_ from '../../containers/Layout/Bubble'

const ExpandButton = ({ text, ...props }) => (
  <Button {...props} width={'2.5em'}>
    {text}
  </Button>
)

const Wrapper = styled(Bubble_)`
  padding: 0;
`

const FilterContainer = styled(Bubble_)`
  background-color: ${themeGet('colors.primary')};
`

const Container = styled.div`
  width: 100%;
  display: flex;
  justify-content: space-between;

  * > {
    flex: 1;
  }
`

const Bubble = styled(Bubble_)`
  flex: 1;
  margin-right: 1em;
`

const DetailContainer = styled.div`
  display: flex;
  flex-direction: row;
  transition: height 120ms ease-in-out;
  ${({ isOpen }) =>
    isOpen
      ? css`
          height: 10em;
        `
      : css`
          height: 0em;
          visibility: hidden;
        `}
`
const Form_ = ({ children, ...props }) => <form {...props}>{children}</form>

const FilterBar = ({
  defaultOpen = true,
  searchFilters,
  extendedFilters,
  onDetailChange,
  onDetailToggleEffect,
  Form,
}) => {
  const [isOpen, setOpen] = useState(defaultOpen)
  const FormComponent = Form || Form_
  return (
    <Wrapper>
      <FormComponent>
        <FilterContainer>
          <Container>
            {searchFilters.map((c, key) => (
              <Bubble key={key}>{c}</Bubble>
            ))}
            {extendedFilters && (
              <ExpandButton
                onClick={e => {
                  e.preventDefault()
                  onDetailToggleEffect && onDetailToggleEffect()
                  if (onDetailChange) {
                    onDetailChange(isOpen, setOpen)
                  } else {
                    setOpen(!isOpen)
                  }
                }}
                text={isOpen ? 'x' : '...'}
              />
            )}
          </Container>
        </FilterContainer>
        <DetailContainer isOpen={isOpen}>
          {extendedFilters &&
            extendedFilters.map((c, key) => <Bubble key={key}>{c}</Bubble>)}
        </DetailContainer>
      </FormComponent>
    </Wrapper>
  )
}

FilterBar.propTypes = {
  defaultOpen: propTypes.bool,
  searchFilters: propTypes.array,
  extendedFilters: propTypes.array,
  onDetailChange: propTypes.func,
  onDetailToggleEffect: propTypes.func,
  Form: propTypes.elementType,
}

export default FilterBar
