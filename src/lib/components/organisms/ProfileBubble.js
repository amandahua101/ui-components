import React from 'react'
import styled from 'styled-components'
import themeGet from '@styled-system/theme-get'
import PropTypes from 'prop-types'

import Bubble from '../../containers/Layout/Bubble.js'
import FiveStarRating from '../molecules/FiveStarRating.js'
import Heart from '../atoms/Icons/Heart.js'
import Icon from '../atoms/Icon.js'
import Lens from '../atoms/Shape/Lens.js'
import PinShape from '../atoms/Shape/PinShape.js'
import Request from '../atoms/Icons/Request.js'
import Text from '../atoms/Text.js'

const LensShadow = styled.div`
  & svg {
    filter: drop-shadow(0px 2px 4px rgba(0, 0, 0, 0.2))
      drop-shadow(0px 1px 10px rgba(0, 0, 0, 0.12))
      drop-shadow(0px 4px 5px rgba(0, 0, 0, 0.14));
  }
`

const BubblePart = styled.div`
  display: flex;
  flex-direction: column;
  align-items: start;
`

const Left = styled(BubblePart)`
  justify-self: start;
`

const Middle = styled(BubblePart)`
  margin: 0 ${themeGet('spacing.medium')};
  justify-content: center;
  justify-self: center;
`

const Right = styled(BubblePart)`
  justify-self: end;
`

const Section = styled.h2`
  font-weight: normal;
  text-transform: lowercase;
  font-size: ${themeGet('fontSizes.h2')};
  margin: 0;
  font-family: ${themeGet('fonts.title')};
  color: ${themeGet('colors.primary')};
`

const Flex = styled.div`
  display: flex;
  flex-flow: row wrap;
  align-items: center;
`

const SpacerSpan = styled.span`
  margin: 0 ${themeGet('spacing.small')};
`

const ProfilePicture = styled.img`
  border-radius: 2000rem;
  max-width: 100%;
`

const Spacer = () => <SpacerSpan>|</SpacerSpan>

const ProfileBubble = ({
  name = 'Linguala',
  languageCount = 5,
  rating = 5,
  servicesIDs = ['translation', 'interpreting', 'lessons'],
  pictureURL,
  location: { name: locationName, latlng } = {
    name: 'Basel, CH',
    latlng: [47.53667, 7.60854],
  },
  contact = 'Kontaktaufnahme',
  favorite = 'Zu Favoriten hinzufügen',
}) => (
  <Bubble
    shadow={'low'}
    row
    stretch
    style={{
      margin: `${themeGet('spacing.medium')} ${themeGet('spacing.large')}`,
    }}
  >
    <Left>
      <LensShadow>
        <Lens size={7} style={{ marginBottom: '-1em' }}>
          <ProfilePicture src={pictureURL} alt={'Profile picture'} />
        </Lens>
      </LensShadow>
      <PinShape size={7} rotation={180} style={{ marginTop: '-1em' }}>
        <Text>{'Map'}</Text>
      </PinShape>
    </Left>
    <Middle>
      <Section>{name}</Section>
      <Flex>
        <Text>{locationName}</Text>
        <Spacer />
        <Text primary>{languageCount} Sprachen</Text>
        <Spacer />
        <FiveStarRating rating={rating} />
      </Flex>
      <Flex>
        {servicesIDs &&
          servicesIDs.map((serviceId, index) => (
            <React.Fragment key={index}>
              <Text>{serviceId}</Text>
              {index < servicesIDs.length - 1 && <Spacer />}
            </React.Fragment>
          ))}
      </Flex>
    </Middle>
    <Right>
      <PinShape size={6} rotation={90} style={{ marginBottom: '-1em' }}>
        <Icon icon={Request} alt={contact} />
      </PinShape>
      <Text size={4}>{contact}</Text>
      <PinShape size={6} rotation={90} style={{ marginBottom: '-1em' }}>
        <Icon icon={Heart} alt={favorite} />
      </PinShape>
      <Text size={4}>{favorite}</Text>
    </Right>
  </Bubble>
)

ProfileBubble.propTypes = {
  profile: PropTypes.object,
}

export default ProfileBubble
