import React from 'react'
import styled from 'styled-components'
import PropTypes from 'prop-types'
import themeGet from '@styled-system/theme-get'

const headingHeight = '33px'
const shadowWidth = '7px'

const MenuWrapper = styled.div`
  display: block;
  position: relative;
  text-transform: lowercase;
  font-family: ${themeGet('fonts.title')};
`

const Wrapper = styled.div`
  float: left;
`

const HeadingWrapper = styled(Wrapper)`
  display: flex;
  position: absolute;
  flex-direction: row-reverse;
  flex-wrap: no-wrap;
  align-items: start;
  transform-origin: top left;
  transform: rotate(-90deg) translate(50px);
  bottom: 0;
  left: 0;
  height: ${headingHeight};
`

const HeadingBackground = styled.h3`
  position: relative;
  height: 100%;
  background-color: white;
  border-top-right-radius: 50px;
  border-top-left-radius: 50px;
  margin: 0;
  padding: 0 ${themeGet('spacing.medium')};
  box-shadow: ${themeGet('shadows.button')};

  :before {
    content: '';
    position: absolute;
    background-color: white;
    width: calc(100% + ${shadowWidth} + ${shadowWidth});
    height: ${shadowWidth};
    bottom: -${shadowWidth};
    left: -${shadowWidth};
  }
`

const Item = styled.div`
  color: ${({ selected }) =>
    themeGet(selected ? 'colors.secondary' : 'colors.primary')};
`

const Heading = styled(Item)`
  padding: ${themeGet('spacing.small')};
  margin: 0;
  font-weight: normal;
  ${({ selected }) => !selected && 'opacity: 0.4;'}
`

const EntryWrapper = styled(Wrapper)`
  background-color: white;
  border-radius: 100px;
  padding: ${themeGet('spacing.medium')};
  margin-left: ${headingHeight};
  box-shadow: ${themeGet('shadows.button')};
  ${({ minHeight }) => minHeight && `min-height: ${minHeight};`}
`

const Entry = styled(Item)`
  margin: ${themeGet('spacing.large')} 0;
`

// tagged template literal

const Menu = ({ headings, entries, minHeight }) => (
  <MenuWrapper>
    <HeadingWrapper>
      {headings.map((heading, index) =>
        heading.selected ? (
          <HeadingBackground key={index}>
            <Heading selected={heading.selected}>{heading.item}</Heading>
          </HeadingBackground>
        ) : (
          <Heading key={index} selected={heading.selected}>
            {heading.item}
          </Heading>
        )
      )}
    </HeadingWrapper>
    <EntryWrapper minHeight={minHeight}>
      {entries.map((entry, index) => (
        <Entry key={index} selected={entry.selected}>
          {entry.item}
        </Entry>
      ))}
    </EntryWrapper>
  </MenuWrapper>
)

Menu.propTypes = {
  headings: PropTypes.array,
  entries: PropTypes.array,
  minHeight: PropTypes.string,
}

export default Menu
