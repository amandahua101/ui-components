import styled from 'styled-components'

const Block = styled.div`
  width: 100%;

  :focus {
    width: 100%;
  }
`

export default Block
