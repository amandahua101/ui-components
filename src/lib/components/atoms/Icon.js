import React from 'react'
import PropTypes from 'prop-types'

import { unit } from '../../utils'

/**
 * Extend Icon to style it.
 *
 * styled(Icon)`
 * change color on hover
 * do a barrel roll
 * `
 */
const Icon = props => {
  return (
    <props.icon
      style={{ width: unit(props.size || 4) }}
      role="img"
      aria-label={props.alt}
      {...props}
    />
  )
}

Icon.propTypes = {
  icon: PropTypes.func.isRequired,
  alt: PropTypes.string.isRequired,
  size: PropTypes.number,
}

export default Icon
