import React from 'react'
import styled from 'styled-components'
import themeGet from '@styled-system/theme-get'
import PropTypes from 'prop-types'

import { unit } from '../../utils/unit'

import AsteriksCircle from './Icons/AsteriksCircle'
import Checkmark from './Icons/Checkmark'
import ErrorIcon from './Icons/ErrorIcon'
import FormWrapper from './FormWrapper'
import Icon from './Icon'
import SelectedLens from './Icons/SelectedLens'
import ToggleArrow from './Icons/ToggleArrow'
import UnselectedLens from './Icons/UnselectedLens'

const StyledSelector = styled.div`
  position: relative;
  margin: ${themeGet('spacing.medium')};
  display: flex;
  flex-direction: column;
  width: ${props => unit(props.size || 10)};
  cursor: default;
  outline: 0;
`

const StyledHeader = styled.div`
  height: 100%;
  width: 100%;
  position: relative;
  box-shadow: ${themeGet('shadows.low')};
  border: none;
  border-top-left-radius: ${unit(1)};
  border-top-right-radius: ${unit(1)};
  border-bottom-left-radius: ${props => (props.isDropdownOpen ? 0 : unit(1))};
  border-bottom-right-radius: ${props => (props.isDropdownOpen ? 0 : unit(1))};
  background: ${props =>
    props.background && props.isDropdownOpen
      ? themeGet('colors.secondary')
      : props.background && !props.isDropdownOpen
      ? themeGet('colors.primary')
      : 'white'};
`

const StyledDropdown = styled.div`
  :before {
    background: ${props =>
      props.background && props.isDropdownOpen
        ? themeGet('colors.secondary')
        : props.background && !props.isDropdownOpen
        ? themeGet('colors.primary')
        : 'white'};
    content: '.';
    font-size: 0;
    width: 100%;
    height: 2px;
    position: absolute;
    top: -2px;
  }
  position: ${props => (props.isDropdownOpen ? 'absolute' : 'relative')};
  z-index: 10;
  top: 100%;
  background: ${props =>
    props.background ? themeGet('colors.secondary') : 'white'};
  border: none;
  color: ${themeGet('colors.primary')};
  display: flex;
  flex-direction: column;
  box-shadow: ${themeGet('shadows.low')};
  border: none;
  border-bottom-left-radius: ${unit(1)};
  border-bottom-right-radius: ${unit(1)};
  width: 100%;
`

const StyledOptionWrapper = styled.div`
  max-height: ${props => unit(props.height || 12)};
  overflow: auto;
  background: white;
  margin: ${themeGet('spacing.small')};
  border-radius: ${themeGet('border.radius')};
  padding: ${themeGet('spacing.small')};
  box-shadow: ${props =>
    !props.background ? 'none' : themeGet('shadows.low')};
`
const StyledOption = styled.div`
color: ${props =>
  themeGet(props.selected ? 'colors.secondary' : 'colors.primary')}
padding: ${themeGet('spacing.small')};
display: flex;
align-items: center;
border: none;
border-radius: ${props => (props.shadow ? themeGet('border.radius') : '0')};
:hover {
  color: ${themeGet('colors.secondary')};
}
`

const StyledTitle = styled.div`
  color: ${props => {
    if (props.background) {
      return 'white'
    } else if (props.optionsSelected) {
      if (props.optionsSelected.length > 0) {
        return themeGet('colors.secondary')
      }
    }
  }};
  text-align: center;
  padding: ${themeGet('spacing.small')} ${themeGet('spacing.small')} 0
    ${themeGet('spacing.small')};
  width: 100%;
  outline: 0;
`

const StyledOptionText = styled.div`
  width: 100%;
`

const StyledRequiredIcon = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  z-index: 1;
`

const StyledPlaceholderIcon = styled.div`
  transform: ${props =>
    props.isDropdownOpen && !(props.errors === false)
      ? 'rotate(180deg)'
      : null};
`

function RequiredIcon(props) {
  if (props.required || props.errors) {
    return (
      <StyledRequiredIcon>
        <Icon
          icon={
            props.errors ? ErrorIcon : props.required ? AsteriksCircle : null
          }
          alt={
            props.errors
              ? 'Error Icon'
              : props.required
              ? 'Required Icon'
              : null
          }
          size={1}
          colorPrimary={
            props.errors === false
              ? themeGet('colors.secondary')
              : props.errors
              ? themeGet('colors.danger')
              : themeGet('colors.idle')
          }
        />
      </StyledRequiredIcon>
    )
  }
  return null
}

function PlaceholderIcon(props) {
  return (
    <StyledPlaceholderIcon {...props}>
      <Icon
        icon={props.errors === false ? Checkmark : ToggleArrow}
        size={0.5}
        alt={props.errors === false ? 'Checkmark' : 'Toggle Arrow'}
        colorPrimary={props.background === true ? 'white' : null}
        colorSecondary={props.background === true ? 'white' : null}
      />
    </StyledPlaceholderIcon>
  )
}

function OptionIcon(props) {
  if (props.multiple) {
    return (
      <Icon
        icon={props.selected ? UnselectedLens : SelectedLens}
        alt={props.selected ? 'Unselected' : 'Selected'}
        size={1}
      />
    )
  }
  return null
}

function Header(props) {
  const placeholderText = () => {
    if (props.optionsSelected) {
      if (props.optionsSelected.length > 0) {
        return props.optionsSelected
          .map(optionIndex => props.options[optionIndex])
          .join(', ')
      }
    }
    return props.placeholder
  }

  return (
    <React.Fragment>
      <RequiredIcon {...props} />
      <StyledHeader onClick={props.handleToggleDropdown} {...props}>
        <StyledOption {...props}>
          <StyledTitle {...props}>{placeholderText()}</StyledTitle>
          <PlaceholderIcon {...props} />
        </StyledOption>
      </StyledHeader>
    </React.Fragment>
  )
}

function Dropdown(props) {
  if (props.isDropdownOpen) {
    const Options = props.options.map((option, index) => (
      <StyledOption
        key={index}
        index={index}
        selected={
          props.optionsSelected === undefined
            ? false
            : !!props.optionsSelected.includes(index)
        }
        onClick={() => props.handleOptionClick(index)}
      >
        <StyledOptionText>{option}</StyledOptionText>
        <OptionIcon
          {...props}
          selected={
            props.optionsSelected === undefined
              ? false
              : !!props.optionsSelected.includes(index)
          }
        />
      </StyledOption>
    ))
    return (
      <StyledDropdown {...props}>
        <StyledOptionWrapper {...props}>{Options}</StyledOptionWrapper>
      </StyledDropdown>
    )
  }
  return null
}

function StatelessSelector(props) {
  const handleToggleDropdown = () => {
    props.setFormState({
      ...props,
      isDropdownOpen: !props.isDropdownOpen || false,
    })
  }

  const handleCloseDropdown = isDropdownOpen => {
    if (isDropdownOpen) {
      props.setFormState({
        ...props,
        isDropdownOpen: false,
      })
    }
  }

  const handleOptionClick = index => {
    if (props.multiple) {
      if (!props.optionsSelected) {
        props.setFormState({
          ...props,
          optionsSelected: [index],
          errors: false,
        })
      } else if (!props.optionsSelected.includes(index)) {
        props.setFormState({
          ...props,
          optionsSelected: props.optionsSelected.concat(index),
          errors: false,
        })
      } else if (props.optionsSelected.includes(index)) {
        props.setFormState({
          ...props,
          optionsSelected: props.optionsSelected.filter(item => item !== index),
          errors: props.optionsSelected.length > 1 ? false : undefined,
        })
      }
    } else {
      props.setFormState({
        ...props,
        optionsSelected: [index],
        isDropdownOpen: false,
        errors: false,
      })
    }
  }

  return (
    <React.Fragment>
      <StyledSelector tabIndex={0} onBlur={handleCloseDropdown}>
        <Header handleToggleDropdown={handleToggleDropdown} {...props} />
        <Dropdown handleOptionClick={handleOptionClick} {...props} />
      </StyledSelector>
    </React.Fragment>
  )
}

const Selector = props => (
  <FormWrapper
    {...props}
    render={name => <StatelessSelector name={name} {...props} />}
  />
)

StatelessSelector.propTypes = {
  name: PropTypes.string.isRequired,
  placeholder: PropTypes.string,
  options: PropTypes.array,
  optionsSelected: PropTypes.array,
  multiple: PropTypes.bool,
  required: PropTypes.bool,
  size: PropTypes.number,
  height: PropTypes.number,
  errors: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.bool,
    PropTypes.func,
  ]),
  isDropdownOpen: PropTypes.bool,
}

export default Selector
