import React from 'react'
import PropTypes from 'prop-types'

import { unit } from '../../utils'

const Image = ({ alt = 'image', src, size = '10', qubic = false }) => {
  const length = unit(size)
  if (qubic) {
    return <img alt={alt} src={src} width={length} height={length} />
  } else {
    return <img alt={alt} src={src} width={length} />
  }
}

Image.propTypes = {
  alt: PropTypes.string,
  src: PropTypes.string,
  size: PropTypes.string,
  qubic: PropTypes.bool,
}

export default Image
