import React from 'react'
import styled from 'styled-components'
import PropTypes from 'prop-types'
import themeGet from '@styled-system/theme-get'

import FormWrapper from './FormWrapper'
import InputShape from './InputShape'

const StyledInput = styled.input`
  width: 100%;
  border: none;
  border-bottom: 3px solid ${themeGet('colors.idle')};
  background: none;
  font-family: ${props =>
    props.type === 'password' ? 'Linguala-Password' : 'Linguala-Title'};
  font-size: ${themeGet('fontSizes.p')};
  color: ${themeGet('colors.secondary')};
  ::placeholder {
    color: ${props =>
      props.errors === false
        ? themeGet('colors.secondary')
        : props.errors
        ? themeGet('colors.primary')
        : themeGet('colors.idle')};
    opacity: 0.4;
  }
`

function StatelessSingleLineInput(props) {
  const handleFocus = () => {
    props.setFormState({
      ...props,
      active: true,
    })
  }

  const handleBlur = () => {
    props.setFormState({
      ...props,
      active: false,
    })
  }

  const handleChange = event => {
    props.setFormState({
      ...props,
      value: event.target.value,
    })
  }

  return (
    <InputShape {...props}>
      <StyledInput
        type={props.type || 'text'}
        placeholder={props.placeholder}
        value={props.value || ''}
        disabled={props.disabled}
        required={props.required}
        maxLength={props.maxLength}
        onFocus={handleFocus}
        onBlur={handleBlur}
        onChange={handleChange}
      />
    </InputShape>
  )
}

const SingleLineInput = props => (
  <FormWrapper
    {...props}
    render={name => <StatelessSingleLineInput name={name} {...props} />}
  />
)

StatelessSingleLineInput.propTypes = {
  name: PropTypes.string.isRequired,
  setFormState: PropTypes.func,
  value: PropTypes.string,
  size: PropTypes.number,
  label: PropTypes.string,
  placeholder: PropTypes.string,
  errors: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.bool,
    PropTypes.func,
  ]),
  type: PropTypes.string,
}

export default SingleLineInput
