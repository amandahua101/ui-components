import React from 'react'
import styled, { css } from 'styled-components'
import themeGet from '@styled-system/theme-get'
import PropTypes from 'prop-types'

import buttonShape from '../../styles/buttonShape'
import { unit } from '../../utils/unit'

import Colorable from './Colorable'

const popUpStyle = css`
  transform: scale(1.05);
`

const onActiveScale = css`
  :active {
    ${popUpStyle}
  }
`

const StyledColorableShape = styled(Colorable)`
  font-size: ${themeGet('fontSizes.p')};
  padding: ${themeGet('spacing.small')};
  margin: ${themeGet('spacing.small')};
  line-height: 1.5em;
  box-shadow: none;
  user-select: none;
  // BUG-TODO: Make sure text cannot be cropped
  min-height: 2.5em;
  height: ${({ size, height }) => (!height ? unit(size || 1) : height)};
  transition: transform 0.2s ${themeGet('transitions.main')};
  text-transform: lowercase;

  /* make sure outline is not displayed */
  ::-moz-focus-inner {
    border: 0;
  }

  ${({ selected }) =>
    selected &&
    css`
      ${popUpStyle}
      box-shadow: ${themeGet('shadows.low')};
      background-color: ${themeGet('colors.secondary')}
    `}
  ${({ noresize, disabled }) =>
    !disabled && !noresize && onActiveScale}

  :focus, :active {
    box-shadow: ${({ disabled }) => disabled || themeGet('shadows.low')};
    outline: none;
  }

  ${buttonShape}
`

const Button = props => (
  <StyledColorableShape
    size={props.size || 2}
    width={unit(12)}
    selected={props.selected}
    {...props}
  >
    {props.value || props.children}
  </StyledColorableShape>
)

Button.propTypes = {
  danger: PropTypes.bool,
  disabled: PropTypes.bool,
  noresize: PropTypes.bool,
  selected: PropTypes.bool,
  size: PropTypes.number,
  children: PropTypes.node,
  value: PropTypes.string,
}

export default Button
