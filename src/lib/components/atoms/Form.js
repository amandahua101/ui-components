import React, { useEffect, useState } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import FormContext from './FormContext.js'

const StyledForm = styled.form`
  width: 100%;
`

const Form = props => {
  const [state, setState] = useState(props.initFormState)

  useEffect(() => {
    if (props.handleFormChange) {
      props.handleFormChange(state)
    }
  })
  const setFormState = newState => {
    setState(prevState => {
      return { ...prevState, ...newState }
    })
  }

  const handleFormSubmit = event => {
    event.preventDefault()
    if (props.handleFormSubmit) {
      props.handleFormSubmit(state)
    }
  }

  return (
    <StyledForm onSubmit={handleFormSubmit} {...props}>
      <FormContext.Provider value={[state, setFormState]}>
        {props.children}
      </FormContext.Provider>
    </StyledForm>
  )
}

Form.propTypes = {
  initFormState: PropTypes.object,
  handleFormSubmit: PropTypes.func,
  handleFormChange: PropTypes.func,
}

export default Form
