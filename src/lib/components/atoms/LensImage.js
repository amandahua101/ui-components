import React from 'react'
import PropTypes from 'prop-types'

import { colorType, unit } from '../../utils'

const LensImage = ({ size, color, secondary, danger, disabled }) => {
  const length = unit(size)
  return (
    <svg
      width={length}
      height={length}
      viewBox="0 0 1000 1000"
      version="1.1"
      xmlns="http://www.w3.org/2000/svg"
    >
      <title>Atoms/Icon/Pin</title>
      <defs>
        <filter
          x="-4.2%"
          y="-2.5%"
          width="108.5%"
          height="106.9%"
          filterUnits="objectBoundingBox"
          id="filter-1"
        >
          <feMerge>
            <feMergeNode in="SourceGraphic" />
          </feMerge>
        </filter>
      </defs>
      <g
        id="Symbols"
        stroke="none"
        strokeWidth="1"
        fill="none"
        fillRule="evenodd"
      >
        <g id="Atoms/Icon/Pin">
          <g id="Group" transform="translate(1.000000, 0.000000)">
            <g
              id="Atom-Pin"
              filter="url(#filter-1)"
              transform="translate(210.000000, 219.000000)"
              fillRule="nonzero"
              fill={color || colorType({ secondary, danger, disabled })}
            >
              <path
                d="M577.143802,255.995818 C556.506373,107.376739 437.401481,0 289.22782,0 C141.05416,0 23.9375802,107.286714 3.17831886,255.995818 C-35.3577758,532.049383 289.257577,697.727596 289.257577,699.979533 C289.257577,702.23147 613.586498,518.435484 577.143802,255.995818 Z M40,290 C40,152.5 152.5,40 290,40 C427.5,40 540,152.5 540,290 C540,427.5 428.75,540 290,540 C152.5,540 40,427.5 40,290 Z"
                id="Pin-Main-Shape"
              />
            </g>
          </g>
        </g>
      </g>
    </svg>
  )
}

LensImage.propTypes = {
  size: PropTypes.number,
  color: PropTypes.number,
  secondary: PropTypes.bool,
  danger: PropTypes.bool,
  disabled: PropTypes.bool,
}

export default LensImage
