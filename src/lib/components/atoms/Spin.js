import styled, { css } from 'styled-components'
import themeGet from '@styled-system/theme-get'

import { fullRotation, reversefullRotation } from '../../styles/keyframes'
import { centerMixin } from '../../styles/mixins'

const animationMixin = css`
  animation: ${fullRotation};
`

const reverseAnimationMixin = css`
  animation: ${reversefullRotation};
`

const fullMixin = css`
  height: 100%;
  width: 100%;
  margin-left: auto;
  margin-right: auto;
  vertical-align: middle;
`

const Spin = styled.div`
  ${centerMixin}
  ${fullMixin}
  ${({ reverse }) => (!reverse ? animationMixin : reverseAnimationMixin)}
  animation-duration: ${({ fast }) => (fast ? '0.8s' : '2s')};
  animation-timing-function: ${({ wobbly }) =>
    wobbly ? themeGet('transitions.wobbly') : 'linear'};
  animation-iteration-count: infinite;
`

export default Spin
