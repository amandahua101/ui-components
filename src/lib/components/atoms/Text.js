import styled, { css } from 'styled-components'
import themeGet from '@styled-system/theme-get'
import PropTypes from 'prop-types'

import { unit, unitLength } from '../../utils'
import { centerMixin } from '../../styles/mixins'

const fontSize = unitLength(0.3, 'rem')
const Text = styled.p`
  font-size: ${({ size }) =>
    size ? fontSize((size + 1) / 1.5 - 0.5) : themeGet('fontSizes.p')};
  margin: ${themeGet('spacing.small')} auto;
  ${({ primary }) =>
    primary &&
    css`
      color: ${themeGet('colors.primary')};
    `}
  ${({ centered }) => centered && centerMixin}
  text-align: left;
  max-width: ${({ widthLimit }) => widthLimit || unit(12)};
  line-height: 1.55em;
`

Text.propTypes = {
  widthLimit: PropTypes.string,
  size: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  centered: PropTypes.bool,
  primary: PropTypes.bool,
}

export default Text
