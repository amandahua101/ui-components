import React from 'react'
import styled from 'styled-components'

import PinImage from '../PinImage'
import Rotate from '../../../containers/Rotate'

import ShapeContent from './ShapeContent'

const innerFactor = 2.6

const OuterDiv = styled.div`
  position: relative;
`

const Pin = ({
  children,
  size = 5,
  rotation = 0,
  shapeColor,
  full,
  border,
  secondary,
  danger,
  disabled,
  style,
}) => (
  <OuterDiv style={style}>
    <Rotate rotation={rotation}>
      <PinImage
        size={size}
        color={shapeColor}
        full={full}
        border={border}
        secondary={secondary}
        danger={danger}
        disabled={disabled}
      />
    </Rotate>
    {children && (
      <ShapeContent size={size} factor={innerFactor}>
        {React.cloneElement(children, {
          size: size / innerFactor,
          centered: true,
        })}
      </ShapeContent>
    )}
  </OuterDiv>
)

export default Pin
