import React from 'react'
import styled from 'styled-components'
import PropTypes from 'prop-types'

import LensImage from '../LensImage'
import Rotate from '../../../containers/Rotate'

import ShapeContent from './ShapeContent'

const innerFactor = 2.6

const OuterDiv = styled.div`
  position: relative;
`

const Lens = ({
  children,
  size = 5,
  rotation = 0,
  shapeColor,
  secondary,
  disabled,
  danger,
  style,
  ...props
}) => (
  <OuterDiv style={style} {...props}>
    <Rotate rotation={rotation}>
      <LensImage
        size={size}
        color={shapeColor}
        secondary={secondary}
        disabled={disabled}
        danger={danger}
      />
    </Rotate>
    {children && (
      <ShapeContent size={size} factor={innerFactor}>
        {React.cloneElement(children, {
          size: size / innerFactor,
          centered: true,
        })}
      </ShapeContent>
    )}
  </OuterDiv>
)

Lens.propTypes = {
  size: PropTypes.number,
}

export default Lens
