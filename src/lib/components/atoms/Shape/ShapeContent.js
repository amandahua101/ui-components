import styled from 'styled-components'
import PropTypes from 'prop-types'

import { centerPosition, unit } from '../../../utils'

const ShapeContent = styled.div`
  display: flex;
  position: absolute;
  top: ${({ factor }) => centerPosition(factor)};
  left: ${({ factor }) => centerPosition(factor)};
  width: ${({ factor, size }) => unit(size / factor)};
  height: ${({ factor, size }) => unit(size / factor)};

  font-size: 0;
`

ShapeContent.propTypes = {
  factor: PropTypes.number,
  size: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
}

export default ShapeContent
