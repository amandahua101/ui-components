import React from 'react'

const SvgPortfolio = props => (
  <svg viewBox="0 0 25 25" {...props}>
    <defs>
      <path id="Portfolio_svg__a" d="M0 0h25v25H0z" />
    </defs>
    <g fill="none" fillRule="evenodd">
      <mask id="Portfolio_svg__b" fill="#fff">
        <use xlinkHref="#Portfolio_svg__a" />
      </mask>
      <path
        d="M12.5 24.5c-6.627 0-12-5.373-12-12s5.373-12 12-12 12 5.373 12 12-5.373 12-12 12z"
        fill="#1CB569"
        fillRule="nonzero"
        mask="url(#Portfolio_svg__b)"
      />
      <path
        d="M20 7h-4V5a1 1 0 0 0-1-1h-4a1 1 0 0 0-1 1v2H6a1 1 0 0 0-1 1v11a1 1 0 0 0 1 1h14a1 1 0 0 0 1-1V8a1 1 0 0 0-1-1zm-8-1h2v1h-2V6zm7 12H7v-4h4v1h4v-1h4v4zm0-5h-4v-1h-4v1H7V9h12v4z"
        fill="#FFF"
        fillRule="nonzero"
      />
    </g>
  </svg>
)

export default SvgPortfolio
