import React from 'react'

const SvgRequest = props => (
  <svg viewBox="0.5 0.5 100 100" fill="none" {...props}>
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M50.5 100.5c27.614 0 50-22.386 50-50S78.114.5 50.5.5s-50 22.386-50 50 22.386 50 50 50zM16.125 73.417V60.552L57.167 19.51a2.579 2.579 0 0 1 3.698 0l9.218 9.22a2.578 2.578 0 0 1 0 3.697L28.99 73.417H16.125zm23.438 0h39.062V63H49.979L39.563 73.417z"
      fill="currentcolor"
    />
  </svg>
)

export default SvgRequest
