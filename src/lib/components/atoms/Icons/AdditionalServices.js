import React from 'react'

const SvgAdditionalServices = props => (
  <svg viewBox="0 0 25 25" fill="none" {...props}>
    <mask
      id="AdditionalServices_svg__a"
      maskUnits="userSpaceOnUse"
      x={0}
      y={0}
      width={25}
      height={25}
    >
      <path fill="#fff" d="M0 0h25v25H0z" />
    </mask>
    <g mask="url(#AdditionalServices_svg__a)">
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M24.5 12.5c0 6.627-5.373 12-12 12s-12-5.373-12-12 5.373-12 12-12 12 5.373 12 12z"
        fill="#1CB569"
      />
    </g>
    <path
      d="M17 8h-3v3h-2V8H9V6h3V3h2v3h3v2zM5 15.665h1.385v4.611H5z"
      fill="#fff"
    />
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M19.068 14.96c-.36.172-2.209 1.038-3.116 1.446a1.08 1.08 0 0 1-1.066.956h-4.299v-.312h4.306a.768.768 0 0 0 .693-.498v-.118a.768.768 0 0 0-.769-.769h-1.772c-1.087 0-1.96-.616-3.13-.616-1.432 0-1.786.506-3.226 1.101v3.462a12.05 12.05 0 0 1 3.074-.395 9.53 9.53 0 0 1 2.77.277c.417.104.857.06 1.246-.125 1.571-.692 4.154-2.077 5.732-2.942 1.322-.782.38-1.855-.443-1.468zM21.587 14.385H7.097L5.79 13h17.107l-1.309 1.385z"
      fill="#fff"
    />
  </svg>
)

export default SvgAdditionalServices
