import React from 'react'

const SvgStarHalf = props => (
  <svg viewBox="0 0 106 100" fill="none" {...props}>
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M67.421 34.842l37.842 3.263L76.579 63l8.579 37-32.526-19.632L20.105 100l8.632-37L0 38.105l37.842-3.21L52.632 0l14.79 34.842zM52.631 21.58v48.947l19.843 12L67.21 60l17.474-15.158-23.052-2-9-21.263z"
      fill="currentcolor"
    />
  </svg>
)

export default SvgStarHalf
