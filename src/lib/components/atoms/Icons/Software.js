import React from 'react'

const SvgSoftware = props => (
  <svg viewBox="0 0 25 25" {...props}>
    <defs>
      <path id="Software_svg__a" d="M0 0h25v25H0z" />
    </defs>
    <g fill="none" fillRule="evenodd">
      <mask id="Software_svg__b" fill="#fff">
        <use xlinkHref="#Software_svg__a" />
      </mask>
      <path
        d="M12.5 24.5c-6.627 0-12-5.373-12-12s5.373-12 12-12 12 5.373 12 12-5.373 12-12 12z"
        fill="#1CB569"
        fillRule="nonzero"
        mask="url(#Software_svg__b)"
      />
      <g fill="#FFF">
        <path d="M12.858 18.96c.262-.246.5-.486.708-.755a5.496 5.496 0 0 0 1.163-3.03 5.554 5.554 0 0 0-4.631-5.888 5.363 5.363 0 0 0-2.167.07c-.11.026-.144.014-.145-.113a83.361 83.361 0 0 0-.026-2.088c-.013-.453-.005-.906-.023-1.359-.034-.82.555-1.546 1.441-1.712a2.1 2.1 0 0 1 1.085.067c.765.263 1.542.488 2.315.729 1.868.583 3.737 1.16 5.606 1.74l1.506.468c.22.069.286.159.286.39v10.088c0 .263-.13.39-.401.389-.261-.001-.396-.135-.396-.4V9.64c0-.504-.004-1.008.003-1.512.002-.129-.03-.188-.162-.229-2.63-.81-5.258-1.627-7.887-2.443-.388-.12-.774-.25-1.163-.368-.396-.12-.774-.1-1.087.206-.25.244-.236.705.016.947.05.05.113.076.179.097l8.26 2.582c.03.01.061.018.093.024.196.037.31.163.31.357 0 1.223.008 2.447-.01 3.67-.01.741-.005 1.483-.01 2.225-.01 1.255-.005 2.51-.007 3.766 0 .33-.012.66-.003.99.007.26-.18.482-.504.379-1.415-.452-2.836-.888-4.254-1.33-.025-.009-.049-.021-.095-.04z" />
        <path d="M4 14.772c.003-2.784 2.23-5.17 5.176-5.175 2.961-.005 5.25 2.427 5.184 5.301-.062 2.714-2.251 5.057-5.18 5.058-2.94 0-5.181-2.384-5.18-5.184zm5.184-1.374c-.768 0-1.385.61-1.386 1.372a1.375 1.375 0 0 0 1.373 1.385 1.38 1.38 0 0 0 1.395-1.379 1.38 1.38 0 0 0-1.382-1.378z" />
        <path d="M10.024 14.778a.855.855 0 0 1-.848.847.852.852 0 0 1-.836-.857.852.852 0 0 1 .838-.84.855.855 0 0 1 .846.85z" />
      </g>
    </g>
  </svg>
)

export default SvgSoftware
