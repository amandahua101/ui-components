import React from 'react'

const SvgItaly = props => (
  <svg viewBox="0 0 25 25" fill="none" {...props}>
    <path
      d="M8.108.838v23.337S-.066 20.135.03 12.015C.865 3.764 8.11.839 8.11.839z"
      fill="#009246"
    />
    <path
      d="M8.108.828S11.204.03 12.838.03c1.633 0 4.73 1.077 4.73 1.077v22.79s-3.168 1.073-4.73 1.073c-1.563 0-4.73-.782-4.73-.782V.828z"
      fill="#fff"
    />
    <path
      d="M16.892.836s7.306 3.131 8.064 10.929c.297 8.132-8.064 12.396-8.064 12.396V.836z"
      fill="#CE2B37"
    />
    <path
      d="M24.95 12.5c0 6.876-5.574 12.45-12.45 12.45C5.624 24.95.05 19.376.05 12.5.05 5.624 5.624.05 12.5.05c6.876 0 12.45 5.574 12.45 12.45z"
      stroke="#fff"
      strokeWidth={0.1}
    />
    <mask
      id="Italy_svg__a"
      maskUnits="userSpaceOnUse"
      x={0}
      y={0}
      width={25}
      height={25}
    >
      <path
        d="M24.95 12.5c0 6.876-5.574 12.45-12.45 12.45C5.624 24.95.05 19.376.05 12.5.05 5.624 5.624.05 12.5.05c6.876 0 12.45 5.574 12.45 12.45z"
        fill="#fff"
        stroke="#fff"
        strokeWidth={0.1}
      />
    </mask>
  </svg>
)

export default SvgItaly
