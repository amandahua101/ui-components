import React from 'react'

const SvgStar = props => (
  <svg viewBox="0 0 106 100" fill="none" {...props}>
    <path
      d="M52.632 80.368L85.158 100l-8.632-37 28.737-24.895-37.842-3.21L52.631 0 37.843 34.895 0 38.105 28.737 63l-8.632 37 32.527-19.632z"
      fill="currentcolor"
    />
  </svg>
)

export default SvgStar
