import React from 'react'

const SvgStarEmpty = props => (
  <svg viewBox="0 0 106 100" fill="none" {...props}>
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M67.421 34.842l37.842 3.263L76.579 63l8.579 37-32.526-19.632L20.105 100l8.632-37L0 38.105l37.842-3.21L52.632 0l14.79 34.842zm-34.58 47.632l19.79-11.948 19.842 12L67.21 60l17.474-15.158-23.053-2-9-21.263-8.947 21.21-23.053 2 17.474 15.158-5.263 22.527z"
      fill="currentcolor"
    />
  </svg>
)

export default SvgStarEmpty
