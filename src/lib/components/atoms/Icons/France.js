import React from 'react'

const SvgFrance = props => (
  <svg viewBox="0 0 25 25" fill="none" {...props}>
    <path
      d="M16.666.714S24.51 4.79 24.99 12.843c-.586 8.148-8.323 11.443-8.323 11.443V.714z"
      fill="#ED2939"
    />
    <path
      d="M7.333 1.2s3.3-1.17 4.941-1.17 4.782.9 4.782.9v23.178s-3.27.862-4.782.862-4.94-1.045-4.94-1.045V1.199z"
      fill="#fff"
    />
    <path
      d="M8.333.714v23.572S.05 20.24.05 12.478C.566 3.885 8.333.714 8.333.714z"
      fill="#002395"
    />
    <path
      d="M24.95 12.5c0 6.876-5.574 12.45-12.45 12.45C5.624 24.95.05 19.376.05 12.5.05 5.624 5.624.05 12.5.05c6.876 0 12.45 5.574 12.45 12.45z"
      stroke="#fff"
      strokeWidth={0.1}
    />
    <mask
      id="France_svg__a"
      maskUnits="userSpaceOnUse"
      x={0}
      y={0}
      width={25}
      height={25}
    >
      <path
        d="M24.95 12.5c0 6.876-5.574 12.45-12.45 12.45C5.624 24.95.05 19.376.05 12.5.05 5.624 5.624.05 12.5.05c6.876 0 12.45 5.574 12.45 12.45z"
        fill="#fff"
        stroke="#fff"
        strokeWidth={0.1}
      />
    </mask>
  </svg>
)

export default SvgFrance
