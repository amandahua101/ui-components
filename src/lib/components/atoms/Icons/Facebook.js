import React from 'react'

const SvgFacebook = props => (
  <svg viewBox="0.5 0.5 100 100" fill="none" {...props}>
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M100.5 50.5c0 27.614-22.386 50-50 50s-50-22.386-50-50 22.386-50 50-50 50 22.386 50 50zm-45.476-.06h8.546l1.131-10.994h-9.676V32.97c0-2.429 1.605-2.999 2.746-2.999h6.96v-10.68l-9.592-.041c-10.645 0-13.064 7.974-13.064 13.066v7.12h-6.158V50.44h6.158v31.31h12.949V50.44z"
      fill="currentcolor"
    />
  </svg>
)

export default SvgFacebook
