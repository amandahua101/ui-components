import React from 'react'

const SvgWidgets = props => (
  <svg viewBox="0.5 0.5 100 100" fill="none" {...props}>
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M100.5 50.5c0 27.614-22.386 50-50 50s-50-22.386-50-50 22.386-50 50-50 50 22.386 50 50zM66.387 15.083L46.856 34.58l19.53 19.53H53.758v27.606h27.605V54.11H66.387l19.53-19.53-19.53-19.497zM19.25 47.21V19.604h27.605v27.605H19.25zm0 34.507h27.605V54.11H19.25v27.606z"
      fill="currentcolor"
    />
  </svg>
)

export default SvgWidgets
