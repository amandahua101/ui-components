import React from 'react'

const SvgAdd = props => (
  <svg viewBox="0 0 25 25" {...props}>
    <defs>
      <path id="Add_svg__a" d="M0 0h25v25H0z" />
    </defs>
    <g fill="none" fillRule="evenodd">
      <mask id="Add_svg__b" fill="#fff">
        <use xlinkHref="#Add_svg__a" />
      </mask>
      <path
        d="M12.5 24.5c-6.627 0-12-5.373-12-12s5.373-12 12-12 12 5.373 12 12-5.373 12-12 12z"
        fill="#1CB569"
        fillRule="nonzero"
        mask="url(#Add_svg__b)"
      />
      <path
        fill="#FFF"
        fillRule="nonzero"
        d="M18.75 13.75h-5v5h-2.5v-5h-5v-2.5h5v-5h2.5v5h5z"
      />
    </g>
  </svg>
)

export default SvgAdd
