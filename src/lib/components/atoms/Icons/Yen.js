import React from 'react'

const SvgYen = props => (
  <svg viewBox="0 0 25 25" fill="none" {...props}>
    <mask
      id="Yen_svg__a"
      maskUnits="userSpaceOnUse"
      x={0}
      y={0}
      width={25}
      height={25}
    >
      <path fill="#fff" d="M0 0h25v25H0z" />
    </mask>
    <g mask="url(#Yen_svg__a)">
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M24.5 12.5c0 6.627-5.373 12-12 12s-12-5.373-12-12 5.373-12 12-12 12 5.373 12 12z"
        fill="#1CB569"
      />
    </g>
    <path
      d="M8 6l3.95 7H9.228v1h3v1h-3v1h3v3h1v-3h3v-1h-3v-1h3v-1h-2.797l4.013-7h-1.152l-3.602 6.28L9.148 6H8z"
      fill="#fff"
      stroke="#fff"
      strokeWidth={0.2}
    />
  </svg>
)

export default SvgYen
