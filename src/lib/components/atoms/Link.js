import styled from 'styled-components'

import linkColor from '../../styles/linkColor'

const Link = styled.a`
  ${linkColor}
`

export default Link
