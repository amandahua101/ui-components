import React from 'react'
import styled from 'styled-components'

const Wrapper = styled.div`
  position: relative;
  top: ${({ size }) => `${size / 12}em`};
`

const TriContainer = styled.div`
  overflow: hidden;
  width: ${({ size }) => `${size}em`};
  height: ${({ size }) => `${size}em`};
  display: flex;
  justify-content: center;
  align-items: center;
`

const ShiftUp = styled.div`
  margin-top: 0;
  margin-bottom: ${({ size }) => `${size}em`};
  margin-left: ${({ size }) => `${(size / 3) * -1.9}em`};
  margin-right: ${({ size }) => `${(size / 3) * -1.9}em`};
`

const TriShape = ({ Component, left, middle, right, initSize = 6 }) => {
  const size = initSize / 2
  const angle = 45
  return (
    <Wrapper size={initSize}>
      <TriContainer size={initSize}>
        <Component rotation={-angle} size={size}>
          {left}
        </Component>
        <ShiftUp size={size}>
          <Component rotation={0} size={size}>
            {middle}
          </Component>
        </ShiftUp>
        <Component rotation={angle} size={size}>
          {right}
        </Component>
      </TriContainer>
    </Wrapper>
  )
}

export default TriShape
