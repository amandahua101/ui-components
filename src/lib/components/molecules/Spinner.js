import React from 'react'
import styled from 'styled-components'

import theme from '../../styles/theme.js'
import { Spin } from '../../index.atoms.js'

const Wrap = styled(Spin)`
  background: white;
  border: 1px solid ${theme.colors.primary};
  border-radius: 50%;
  height: 80px;
  width: 80px;
`
const Spinner = ({ children }) => <Wrap reverse>{children}</Wrap>

export default Spinner
