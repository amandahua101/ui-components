import React from 'react'

import { Icon, Star, StarEmpty, StarHalf } from '../../index.atoms'

const FiveStarRating = ({ rating, size = 1 }) => {
  const ratingLimit = rating - 0.5
  return (
    <>
      {Array.from({ length: 5 }, (v, i) => (
        <Icon
          alt={`${i + 1}`}
          key={i}
          icon={i > ratingLimit ? StarEmpty : i < ratingLimit ? Star : StarHalf}
          size={size}
        />
      ))}
    </>
  )
}
export default FiveStarRating
