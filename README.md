# ui-components

All public linguala components that can be published on npm

## playground

[![Edit angry-hofstadter-1iojy](https://codesandbox.io/static/img/play-codesandbox.svg)](https://codesandbox.io/s/angry-hofstadter-1iojy?fontsize=14)

## develop

### requirements

- git & setup ssh-key
- npm & node (recommended to be installed with [nvm](https://github.com/nvm-sh/nvm/blob/master/README.md#installation-and-update))

### initial setup

0. get a gitlab account on [gitlab.linguala.com](https://gitlab.linguala.com/users/sign_in#register-pane)
1. `git clone ssh://git@gitlab.linguala.com:1022/linguala/ui-components.git`
1. `cd ui-components/`
1. `nvm use` #uses the node version defined in .nvmrc
1. `npm i` #installs all dependencies

### start developing

1. `npm run dev` #starts storybook & runs all tests in the background
2. your browser should open http://localhost:9001 where storybook is started

alternatively you can run

- `npm run storybook` by itself
- `npm test` to just run tests once (or `npm run test`)
- `npm run test:watch` to rerun tests on changed files

### contribute

- Read [CONTRIBUTING.md](CONTRIBUTING.md)
