import React from 'react'
import { renderToString } from 'react-dom/server'
import test from 'jest-t-assert'

import { Button } from '../src/lib/index.js'

const data = {
  danger: true,
}

test('ssr', t => {
  const s = renderToString(<Button {...data}>click me</Button>)
  t.true(s.includes('click me'))
})
