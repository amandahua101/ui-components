// This file is for snapshot testing with Jest only
import React from 'react'
import renderer from 'react-test-renderer'
import test from 'jest-t-assert'

import { Text } from '../src/lib/'

test('renderLingualaIcon component', t => {
  const tree = renderer
    .create(<Text>Testing Jest Snapshot Testing</Text>)
    .toJSON()
  t.snapshot(tree)
})

// describe('Toggle', () => {
//   it('renders Toggle component correctly', () => {
//     const tree = renderer
//       .create(<Toggle />)
//       .toJSON()
//     expect(tree).toMatchSnapshot()
//   })
// })
