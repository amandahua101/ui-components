# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.6.0 - 2020-03-11]

### Added

- [Organism]: extendable FilterBar

### Changed

- [npm] hotfix: use commonjs in package.json
- [Icon]: Refactor Icon wrapper (breaks default hover color change)
- [Icon]: Add size prop type
- [Lens]: pass all props to Lens to enable styling with styled-components

## [1.5.0 - 2020-01-27]

### Added

- [storybook]: addon-docs (replaces addon-info)

### Changed

- [npm]: updated packages, mainly: babel 7.8 (**ES2020 support**), storybook 5.3 (**Component Story Format**)
- [storybook]: move to declarative configuration (**.storybook/main.js**), converted all stories to new CSF
- [stories]: split shape stories into `shape/{pin,lens}.stories.js` files in subfolder (only single default exports are allowed)

### Removed

- [storybook]: addon-info, @dump247/storybook-state

## [1.4.0 - 2020-01-22]

### Added

- [Atom] Spin: component that spins its' child component with different rotation speed, style and direction, implemented with keyframes
- [Molecule] Spinner: wrapper for spinning a Component as loading indication (WIP)

### Changed

- [style/mixins]: move centering style to centerMixin from Text
- [Text]: use centerMixin from style/mixins

## [1.3.1 - 2020-01-08]

### Changed

- [theming] set font size on root(html) element
- [node] fix circular imports in ProfileBubble

## [1.3.0 - 2019-12-19]

### Added

- [npm] add @storybook/addon-info
- [npm] add @storybook/addon-centered

### Changed

- [npm] update @linguala/common-dev-dependencies
- [jest] ignore css files
- [storybook] replace custom centerDecorator with @storybook/addon-centered
- [theming]
  Added default font color defaultText to theme
  add fontSizes and spacings to theme
  refactor components to use theme vlues for font-size, padding and margin
  corrected bodystyles

## [1.2.1] - 2019-12-03

### Changed

- [Form]: allow passing of props
- [rollup]: fix minified bundle

## [1.2.0] - 2019-11-21

### Added

- Added LICENSE file
- [Organism] ProfileBubble

### Changed

- [Atom] Bubble: stretch aligned items
- [Atom] Shapes: allow style override on Lens & PinShape
- [Molecule] FiveStarRating: add size parameter, initialize with 1

## [1.1.0] - 2019-11-19

### Added

- [Molecule] FiveStar

### Changed

- Moved Components into atoms/, molecules/ & organisms/
- fixed naming of stories to reflect categories

## [1.0.0] - 2019-11-19

### Added

- Migrated Components from ui-atoms@0.21.0
- Migrated Components from ui-molecules@0.1.1
